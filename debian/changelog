laby (0.7.0-1) unstable; urgency=medium

  * Team upload
  * New upstream release
    - this version uses lablgtksourceview3
    - add support for Go and PHP
  * Use upstream desktop file
  * Bump Standards-Version to 4.5.0
  * Bump debhelper compat level to 13

 -- Stéphane Glondu <glondu@debian.org>  Thu, 02 Jul 2020 09:59:10 +0200

laby (0.6.4-3) unstable; urgency=medium

  * Team upload
  * Update Vcs-*
  * Fix compilation with OCaml 4.08.0

 -- Stéphane Glondu <glondu@debian.org>  Tue, 03 Sep 2019 15:23:14 +0200

laby (0.6.4-2) unstable; urgency=medium

  * Team upload
  * Add ocamlbuild to Build-Depends
  * Bump debhelper compat to 10
  * Update Vcs-*

 -- Stéphane Glondu <glondu@debian.org>  Thu, 20 Jul 2017 13:45:13 +0200

laby (0.6.4-1) unstable; urgency=medium

  * New upstream release.

 -- Mehdi Dogguy <mehdi@debian.org>  Sun, 17 Jan 2016 22:51:52 +0100

laby (0.6.3-1) unstable; urgency=low

  * New upstream release
  * Switch to 3.0 (quilt) source format.
  * Bump Standards-Version to 3.9.2, no changes required.

 -- Mehdi Dogguy <mehdi@debian.org>  Mon, 26 Sep 2011 14:09:54 +0200

laby (0.5.5-1) unstable; urgency=low

  * New upstream release
    - Uses lablgtk2 (>= 2.14) which brings bindings for gtksourceview2
      (Closes: #541958).
  * Use new features of dh-ocaml: automatic computation of dependencies

 -- Mehdi Dogguy <mehdi@debian.org>  Tue, 01 Dec 2009 23:40:22 +0100

laby (0.5.4-1) unstable; urgency=low

  * New upstream release
    + Prolog mode
  * Choose the same icon as upstream
  * Add executable bit to all '/usr/share/laby/mods/*/lib/defs'
  * Bump debhelper's version to (>= 7.0.50~) in build-dependencies
  * Bump standards version to 3.8.3
  * Build-depend on ocaml-findlib (used to be a dependency of
    liblablgtksourceview) (Closes: #549830).

 -- Mehdi Dogguy <mehdi@debian.org>  Sat, 12 Sep 2009 11:35:09 +0200

laby (0.5.3-1) unstable; urgency=low

  * New Upstream Version
  * Remove DMUA, not needed anymore
  * Use my Debian address for the maintainer field

 -- Mehdi Dogguy <mehdi@debian.org>  Mon, 24 Aug 2009 11:45:11 +0200

laby (0.5.2-1) unstable; urgency=low

  * New Upstream Version
    - Ruby mode
    - New uptream homepage
  * Add ruby to the list of compilers in the dependency field and as
    a suggestion.
  * Update watch file
  * Update homepage field
  * Update the category in menus (menu, desktop): LogicGame
  * Bump standards version to 3.8.2
  * Fix permission of run/*/command files (add executable bit)

 -- Mehdi Dogguy <dogguy@pps.jussieu.fr>  Tue, 04 Aug 2009 14:14:54 +0200

laby (0.5.1-1) unstable; urgency=low

  * New Upstream Version
  * Update manpage: add --tile-size
  * Update OCaml version in build-deps

 -- Mehdi Dogguy <dogguy@pps.jussieu.fr>  Fri, 05 Jun 2009 12:43:47 +0200

laby (0.5.0-1) unstable; urgency=low

  * New Upstream Version
  * Remove menhir from build-dependencies: not used.
  * alsa-utils is now a Recommends: used only for sound effects.

 -- Mehdi Dogguy <dogguy@pps.jussieu.fr>  Mon, 01 Jun 2009 21:35:00 +0200

laby (0.4.5-2) unstable; urgency=low

  * Fix FTBFS on non-native architectures: compiling using OCAML_BEST.
  * Add gbp.conf to force use of pristine-tar.

 -- Mehdi Dogguy <dogguy@pps.jussieu.fr>  Sat, 23 May 2009 10:58:54 +0200

laby (0.4.5-1) unstable; urgency=low

  * Initial release (Closes: #515617)

 -- Mehdi Dogguy <dogguy@pps.jussieu.fr>  Wed, 13 May 2009 18:56:57 +0200
